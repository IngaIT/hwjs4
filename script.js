let firstNumber = +prompt("Enter first number")
let secondNumber = +prompt("Enter second number")
while (typeof firstNumber !== 'number' || Number.isNaN(firstNumber) || typeof secondNumber !== 'number' || Number.isNaN(secondNumber)){
  firstNumber = +prompt("Enter first number again")
  secondNumber = +prompt("Enter second number again")
} 
let operator = prompt("Enter operation sign: + - * /")

function calcResult(firstNum, secondNum, operations) {
  switch (operations) {
    case '+':
      return firstNum + secondNum;
    case '-':
      return firstNum - secondNum;
    case '*':
      return firstNum * secondNum;
    case '/':
      return firstNum / secondNum;
  }
}

console.log(calcResult(firstNumber, secondNumber, operator));